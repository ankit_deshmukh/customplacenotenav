#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface MyPlugin : NSObject
{
    NSDate *creationDate;
    AVSpeechSynthesizer *syn;
}
@end

@implementation MyPlugin

static MyPlugin *_sharedInstance;

+(MyPlugin*) sharedInstance
{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSLog(@"Creating MY Plugin Shared instance");
        _sharedInstance = [[MyPlugin alloc] init];
    });
    return _sharedInstance;
}

-(id)init{
    self = [super init];
    if (self)
        [self initHelper];
    return self;
}

-(void)initHelper
{
    NSLog(@"Inithelper start");
    creationDate = [NSDate date];
    syn = [[AVSpeechSynthesizer alloc] init];
    
}

-(double)getElapsedTime{
    return [[NSDate date] timeIntervalSinceDate:creationDate];
}

-(void)speakOut:(NSString*) str1{
    
    AVSpeechUtterance *speechutt = [AVSpeechUtterance speechUtteranceWithString:str1];
    speechutt.volume=90.0f;
    //speechutt.rate=0.50f;
    speechutt.pitchMultiplier=0.80f;
    [speechutt setRate:0.5f];
    speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-us"];
    [syn speakUtterance:speechutt];
}

-(void)stopSpeak{
    [syn stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
}

@end

extern "C"
{
    void TTSSpeaks(char * message){
        [[MyPlugin sharedInstance] speakOut:[NSString stringWithUTF8String: message]];
    }
    void Stopspeak(){
        [[MyPlugin sharedInstance] stopSpeak];
    }
}
